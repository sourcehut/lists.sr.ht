This repository contains the code for the sr.ht mailing list service. For
instructions on deploying or contributing to this project, visit the manual
here:

https://man.sr.ht/lists.sr.ht/installation.md
